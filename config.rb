# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :domain_name,   "mtgrzonka.com"
set :title,   "Mateusz Grzonka - Web Developer"

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

activate :directory_indexes

set :relative_links, true

configure :development do
  # activate :livereload # Reload the browser automatically whenever files change
  set :base_url, "" # Setting empty baseurl
end


# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  activate :minify_css
  activate :minify_javascript
  set :build_dir, 'public'
  activate :relative_assets
  activate :imageoptim
end
