'use strict';

var elem = document.getElementById('canvas');
var params = {
    width: window.innerWidth,
    height: window.innerHeight
};
var two = new Two(params).appendTo(elem);

var background = two.makeRectangle(two.width / 2, two.height / 2 - 1, two.width, two.height);
background.fill = '#364261';

var ground = two.makeRectangle(0, two.height, two.width * 2, two.height * 0.4);
ground.fill = '#2B354E';
ground.noStroke();

var grass_line = two.makeLine(0, two.height * 0.8, two.width * 2, two.height * 0.8);
grass_line.stroke = '#00DE73';
grass_line.linewidth = 8;

var bird_body = two.makeEllipse(100, 100, 10, 20);
bird_body.fill = '#856239';
bird_body.noStroke();
bird_body.rotation = 1;

var bird_head = two.makeEllipse(115, 92, 10, 10);
bird_head.fill = '#C8CC7C';
bird_head.noStroke();

var bird_eye = two.makeEllipse(113, 90, 3, 3);
bird_eye.fill = '#ffffff';
bird_eye.noStroke();

var bird_beak = two.makeEllipse(130, 88, 9, 5);
bird_beak.fill = '#FFD900';
bird_beak.noStroke();

var bird_wing = two.makeEllipse(85, 95, 15, 6);
bird_wing.fill = '#664D2F';
bird_wing.noStroke();

var bird_tail = two.makeEllipse(81, 113, 8, 6);
bird_tail.fill = '#664D2F';
bird_tail.noStroke();

var bird = two.makeGroup(bird_body, bird_head, bird_eye, bird_beak, bird_wing, bird_tail);
bird.translation.set(two.width / 10, two.height / 2);

var wing_state = 1; // 1 - up, 2 - down
var bird_speed = {
    x: {
        current: 0,
        max: 1.8,
        min: 1.2
    },
    y: {
        current: 0,
        max: 1,
        min: 0.3
    }
};

var bird_state = 1; // fly up - 1, fly down - 2, fly back - 3 

var grass_quantity_factor = 8;
var grass_count = two.width / grass_quantity_factor;
var grass = [];
var grass_rotation = [];
var rotation_speed = [];

var message = true;

for (var i = 0; i < grass_count; i++) {
    var grass_obj = two.makeLine(i * grass_quantity_factor, two.height * 0.8, i * grass_quantity_factor + Rand(-2, 2), two.height * 0.8 + Rand(-50, 0));
    grass_obj.stroke = '#00DE73';
    grass_obj.linewidth = 5;
    grass[i] = grass_obj;
    grass_rotation[i] = 0;
    while (grass_rotation[i] === 0) {
        grass_rotation[i] = Math.round(Rand(-1, 1));
    }
    rotation_speed[i] = Rand(0.003, 0.007);
}

two.bind('update', function (frameCount) {

    var index = 0;
    grass.forEach(function (element) {
        if (grass_rotation[index] === 1) {
            element.rotation += rotation_speed[index];
            element.translation.x += 0.1;

            if (element.rotation >= Rand(0.5, 1)) {
                grass_rotation[index] = -1;
            }
        } else if (grass_rotation[index] === -1) {
            element.rotation -= rotation_speed[index];
            element.translation.x -= 0.1;

            if (element.rotation <= Rand(-2, 0.5)) {
                grass_rotation[index] = 1;
            }
        }

        index++;
    });

    if (bird_state === 1) {

        if (wing_state === 1) {
            bird_wing.rotation += 0.02;
            bird_wing.translation.x -= 0.0377;
            bird_wing.translation.y -= 0.0377;

            if (bird_wing.rotation >= 0.3) {
                wing_state = 2;
                bird_speed.x.current = bird_speed.x.min;
                bird_speed.y.current = bird_speed.y.max;
            }
        } else if (wing_state === 2) {
            bird_wing.rotation -= 0.08;
            bird_wing.translation.x += 0.15;
            bird_wing.translation.y += 0.15;

            if (bird_wing.rotation <= -0.6) {
                wing_state = 1;
                bird_speed.x.current = bird_speed.x.max;
                bird_speed.y.current = bird_speed.y.min;
            }
        }

        bird.translation.x += bird_speed.x.current;
        bird.translation.y -= bird_speed.y.current;
    } else if (bird_state === 2) {
        bird_wing.rotation = 0;

        bird.translation.x += bird_speed.x.max;
        bird.translation.y += bird_speed.y.max;
    } else if (bird_state === 3) {

        bird.rotation += 2 * Math.PI;
    }

    if (bird.translation.x >= two.width + 5 || bird.translation.y <= -150) {
        bird_state = 4;
        if (message) {
            console.log("Are you curious what happened to the bird? Well, he flew to search for semicolons.");
            message = false;
        }
    }
}).play();

function Rand(min, max) {
    var random_number = Math.random() * (max - min) + min;
    return random_number;
}